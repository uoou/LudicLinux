---
title: "Grind is Good"
image: title_featured.jpg
subtitle: "Minmax never sleeps"
---

I often hear "it's a grindfest" or "it's too grindy" or similar as a way for a person to dismiss a game without having to go to the effort of expressing what they really dislike about it.

I'm going to argue that there's nothing inherently wrong with grind. Grind can be good. The *overwhelming majority* of games are "*grindy*".

<!--more-->

Like many terms in gaming *grind* is not really very well defined but *we know it when we see it*. I see it used in two senses.

First:

Say you want to craft a high level item in an MMO — you'll have to spend *many* hours gathering materials and crafting items *you don't even want* in order to level up your crafting so that you can finally actually craft the item. Another word for this sort of grind is *busy work*. The activities involved are not, for the most part, *fun* — the player is only engaging in them to unlock something. This kind of grind is, of course, there to lengthen gameplay.

It's also present in the Ubisofty third-person open-ish-world type games that have collecting and crafting elements. You've done all the missions in an area but you've not yet collected all 50 seashells, or whatever, so you spend hours hunting round for them.

This kind of grind doesn't exist to serve the mechanical reality that the game otherwise establishes and as such it stands out -- it doesn't feel *of a part* with the rest of the game. In both cases the grindy activity is something *other* than what would be considered the core gameplay of the game -- it's a mostly disconnected supplementary acitivity. It's also, in both cases, mechanically very simple -- looking for objects, collecting them and perhaps clicking on them to process them. It doesn't really involve any of the decision-making or dynamism we associate with game mechanics -- it's not very *gamey*. In the case of MMOs it's often a barrier to actual progress and is a way of gating the experience whereas in the case of collecting arbitrary *stuff* it's a tacked-on activity which is like a soft-gate to players who feel the need to *really* complete areas.

Second:

In its looser sense it's 







clicker games - just grind with *reward*

When there's no reward = bad





{% include img.html image="image_name.jpg" caption="Witty caption" %}

{% include img.html image="image_name.jpg" %}

{% include yt.html vidid="6Lo0qD6rOz8" %}


