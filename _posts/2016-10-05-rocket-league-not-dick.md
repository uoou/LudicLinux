---
title: "Rocket League"
image: rl_dick_featured.jpg
subtitle: How to not play like a dick
---

I've got a lot of hours in Rocket League and, while I'm not actually very *good*, I *am* very *experienced*. So I thought I'd share what I've learned.

This is not going to be about how to improve your technical skill — the way to do that is to *practise* lots by playing games and doing training. This is going to be about how to play well as a member of a team and will pertain mainly to 3 vs. 3, since that's what I play.

<!--more-->

Technical skill is important, of course, but I think teamwork is perhaps more important. I've been in countless games where a team with far more skill has lost to technically inferior players because the inferior players *worked together*. So here's what I've learned in my 17.5 million hours of Rocket League about being a *team player*.

## 1. Ignore the scoreboard

Up to a point, at least.

The scoring in Rocket League rewards ball-chasing and ball-chasing makes you a *bad team member*. It also gives a lot of points for *missing* (shots on goal) and for scoring goals, which may seem fair enough but I'll get to that later.

The scoring system *does not reward*:

* Being in position to intercept a counter-attack while your team-mates attack
* Bumping an enemy (and they *are* the enemy) player off the ball or out of position (or demoing)
* Staying on the ground while your team-mate attempts to aerial a threatening high ball away, in case they miss
* Passing the ball (unless it's an assist)

The *only* basic plays the game does reward with points (that is ignoring stuff like aerial touches and juggles) are: Goals, shots, assists and clears. 

To illustrate:

Say I'm in an offensive position with the ball but the defence is in place and prepared. I could take a shot but it's very likely to be saved and that's very likely to result in a counter-attack. The game would *reward* me for doing something *very stupid*, harming my team.

Or, I could lay the ball off or hit it off the back wall to draw the defence out — this may not end up as an assist, there are likely to be more touches before any goal is scored — either way, I either get fewer points or none at all for doing something *very smart* which helps my team.

So, don't pay too much attention to the scoreboard. I'm not saying it doesn't matter at all, nor am I really criticising how it works — they *can't* make it perfect. But if you pay too much attention to your score it will make you play badly. You *know* whether you're playing well or not and that matters more than your score.

## 2. Don't chase the ball

Seriously, *don't chase the fucking ball*. I'd rather play with someone who's playing for the very first time and has literally zero experience than someone with lots of skill who chases the ball. 

As I've said, the scoreboard rewards ball-chasing so lots of people seem to think they're playing well just because they're racking up points every time they hit the ball. They seem to have no sense that they're not hitting the ball *usefully*. Don't be that person.

Most of the time you are not going to be the best person to hit a particular ball. There are three of you so, as a rule of thumb, you're probably going to be the best person to deal with roughly 1/3 of balls.

If you chase the ball around you are doing two really bad things for your team:

*  Hitting the ball poorly when someone else could have hit it well
* Forcing your team-mates to play out of position

You, the ball chaser, may even score lots of goals. You see the scoreboard at the end of the game, you scored 3 goals and have the top score on your team, the rest of your team scored no goals. You lost 4 - 3. Because of your ball chaser mentality, you think you played well and were let down by your team.

What you can't be shown is that, if you had played less selfishly, taking less stupid shots, doing fewer shitty clears, not hitting the ball sideways in midfield for no reason at all, then perhaps your team would've scored 5 goals or conceded only 2 and you would have won.

## 3. Rotations and positioning

Positioning rules in Rocket League are all situational. They are *generally* true but there are times when you should break them. It's hard to un-learn bad habits though so learn to play positionally *first*, then you'll learn how and when you can improvise naturally.

Whichever position you are in if you are not doing one of these two things then you are not helping your team and are probably harming them:

* *Usefully* hitting the ball
* Getting/staying in position to deal with a usefully hit ball (whether offensively or defensively)

Say you're trying to centre the ball for your team around the side wall in the corner. The ball gets intercepted weakly and becomes a dead ball. **Do not hang around near that ball** because you are neither hitting the ball nor getting into a position to receive a usefully hit ball. The *best* that can happen in this situation is that you hit the ball weakly and nothing at all is gained. Meanwhile, you are, because of your shitty positioning, preventing your team-mate from rotating in and hitting the ball with force — if they do so, there'll be no one to accept the centre and score because **you are sitting uselessly under the ball**.

This applies equally to clearing the ball in a defensive position. If you go to clear the ball and it's blocked, becoming dead, you *are no use there any more* — get out of there and let someone else hit it. There's nothing more frustrating when in goal than to see someone sitting in the corner repeatedly hitting the ball weakly when you *know* you could clear it solidly if they would only rotate with you.

To reiterate, if you are not either *playing the ball usefully* or *getting into a position to deal with a played ball* then you are **playing badly**.

Generally speaking you should hit the ball once and then let a team-mate take over as, if they're playing well, they will have been getting into a position to do exactly that. This is not always true — sometimes you will be best placed to continue hitting the ball — but broadly speaking, aim for a passing style, it's much harder to defend against.

#### On the attack

There are three positions:

*  The furthest player forwards, we'll call this "1"
*  The player furthest back, we'll call this "3"
*  The one in the middle, we'll call this "2"

Your position on the pitch dictates your role. If you are closest to goal during the kickoff then you are in position 3 and if you go for either boost or the ball at kickoff then you are an arsehole. Don't. Let the kickoff play out before going anywhere – you don't *need* boost at this point, you have 34 which is enough to get to any ball. Stay in goal and be ready for a surprise shot. When it's clear where the ball is going *then* you can move.

The basic roles of those three positions are:

1. Drive the ball forwards and try to get your team into a scoring position
2. Being in position to receive a pass or intercept a rebound
3. Defending against counter-attack

Once 1 has hit the ball, unless they're best placed to hit it again, they should rotate with whoever is. In an attacking play, 1 should rotate with 2 and they should swap roles. If things go wrong and a counter-attack ensues then 2 should attempt to intercept the ball, 3 should rotate forward to take 2's place and 1 should rotate back to 3. This won't always be exactly right — it depends where the ball goes. The point, though, is that all three players should be constantly rotating to maintain those three roles in such a way that the person in the best position to hit any particular ball can do so in the knowledge that their team-mates will plug the gap.

3's job during all this is to watch the play *very carefully* and push up the field as much as they safely can. Watch the enemy cars; be careful to look at their positions in relation to the ball so you can gauge how far they can hit it — don't go any further forward than that. Your job is to get back to goal if a counter attack happens but also to push forwards if neither 1 nor 2 can go for a shot, in which case 1 or 2 will rotate back to take your place if they're any good.

#### In defence

This can get messy, when you're under attack rotations are likely to fall apart as your position will be less clearly defined. What you *really* want to avoid in a defensive position is two or more people going to clear the same ball as this will leave a hole.

You always want someone in goal, on the ground, as a last resort against a shot. Which player this is will change. The other two players should be attempting to clear the ball, ideally *before* it becomes a shot. When a ball goes up high near your goal, player 1 should be going up for that to get rid of it before it becomes a shot. Same for balls around the wall or along the ground — player 1's job is to prevent that from becoming a shot. If player 1 fails, they should not hang around near the ball, they should rotate to position 3 and 2 should attempt to clear. A conveyor-belt of clearance attempts with the last player always staying down on the ground ready to deal with a shot.

#### Summary

On the attack you want two players on the attack and one ready to both defend against a counter-attack and also ready to attack if needs be. On the defence you want one player clearing the ball, one player ready to take over clearing the ball and one player in goal. And in both cases, whenever someone changes position, another player should rotate back to fill the gap.

Think in terms of hitting the ball once, then rotating back. Then the next player hits the ball and then rotates back and so on. So you're either hitting the ball or getting in a position ready to hit the next ball. It's not always true — there will be many times when it's best to hit the ball multiple times. But thinking in those terms will improve your positioning and allow your team to play more efficiently.

Generally, when the enemy is in possession of the ball, the player in position 1 should *always* attack the ball, even if they can't realistically expect to hit it. You don't want the enemy to control the game, you don't want to give them time and space to do what *they* want to do. You want to force them to hit the ball before they're ready and before their team is in position. Player 1's job, when the enemy is in possession, is to disrupt them and make them fuck up.

## 4. A pass is (often) better than a shot

When you're on the attack — not a counter-attack, that's a different situation — against a prepared defence, it's usually best not to go for a direct shot. This is likely to be saved and a good save is also a clearance. Well done, you just turned an offensive play into a defensive one. What you want to do is force the defence to move — pull them out of position so that your team can take a shot with a chance of actually going in.

Either pass to a team-mate, hit the ball against the back wall at an appropriate angle and speed or send the ball up high to force them to attempt a risky clear. Once the defence is out of position, *then* go for the shot. You'll often get several chances because an out-of-position defence don't make good clearances.

## 5. Trust your team-mates

This relates to all of the above. One of the reasons the ball-chaser chases the ball is because they don't trust their team-mates; they're trying to win singlehandedly.

Play your position *well* and trust your team-mates to play theirs. If you *could* hit a ball but you know someone *should*, from their position, be able to hit it better, *leave it for them*. Trust them to hit the balls they should, trust them to rotate when they should. They won't always deserve that trust but you'll still play better as a team than if you tried to do it all yourself.

And your team-mates will learn. In most situations it'll be obvious what they did wrong.

When a team plays *together* it's a beautiful thing. It makes the game seventeen times more fun.

## Conclusion

You want to win and that's cool, that's what the game's about. But you win or lose as a team, so *play like you're in a team*. It'll be more fun for everyone.

![](http://i.imgur.com/3gHAzLp.jpg)

[Here](https://www.reddit.com/r/RocketLeague/comments/3y73om/huge_link_list_for_beginners_and_advanced_players/)'s a handy list of tips and stuff from [/r/RocketLeague](https://www.reddit.com/r/RocketLeague/)


