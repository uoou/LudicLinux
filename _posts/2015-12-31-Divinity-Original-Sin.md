---
title: "Divinity: Original Sin"
image: divinity_featured.jpg
subtitle: Subtitle abuse edition 
author: uoou 
---

[Divinity: Original Sin - Enhanced edition](http://store.steampowered.com/app/373420/) is a long game with a stupidly long title that we've been waiting for far too long. The devs and publishers, Larian, have treated Linux gamers very poorly over the last couple of years and my goodwill towards them is exhausted. But I'm putting all that aside and looking at the game itself.

And it really is an *excellent game*.

<!--more-->

## Overview

Party based RPGs like this seem to appeal to two distinct desires: one is for a deep, rich, complex narrative, and the other is for mechanically interesting combat and skills. Divinity leans heavily toward the latter — it's a game for people who enjoy self-directed exploration and strategic combat in interesting locations against challenging and varied opponents with just enough narrative to keep you moving forwards.

That's not to say the writing or narrative are lacking; the game's world, its mythology and your place within it are well crafted and intriguing. The writing serves the game rather than the game merely being a window into a narrative — if you're after a Byzantine epic then look elsewhere, perhaps.

Main quests and side quests (of which there are *plenty*) are often interwoven and overlapping, with seemingly unimportant diversions gaining significance as you investigate them and feeding back into the main story. Side quests in Divinity don't feel like the disconnected todo-list of chores as all too often they do in RPGs but like small parts of a bigger picture, each of which reveals something about the world around you. Divinity doesn't over-explain, quests tend not to be explicit "go to X, do Y" instructions, they're more likely to be "X has happened, go find out what's going on", which makes the player feel like they're discovering and making sense of things for themselves, rather than being led around by the hand and having everything spelled out for them.

{% include img.html image="divinity_market.jpg" caption="Legends say those easels once held valuable paintings" %}

I never found myself really caring about the distinction between main and side quests while playing Divinity, I just did what I found most interesting and the story unfolded naturally. This gives the narrative a very organic feel; not like following pre-defined steps and acting out an already-written play but creating my own story through my actions. It's an illusion, of course, but it's an artfully executed one.

## Rivelon

That's where Divinity takes place. Divinity's one of those games that will appeal to people who like to play around with systems. It's a mechanically complex place where a startling number and variety of objects can be interacted with in various ways. It's also a world that doesn't take itself too seriously, at least not all the time. The writers get that medievally-fantasy worlds are inherently *a bit ridiculous* and they don't mind engaging with that. It never descends into tedious wackiness but it is, in the right places, a *bit silly*. For example: you might, after picking up a bucket, wonder whether you can wear it on your head. Divinity will let you, it'll actually be a pretty serviceable helmet, but you will also be blind. That's ridiculous. But it's also exactly what you *should* be able to do in a systemic world.

The lightness of the writing is carried over into the mechanics. It doesn't lampshade things; it doesn't say "lol you put a bucket on your head", it just lets you — the wit lies in making it blind you. Similarly the game doesn't suggest how to play around with the mechanics, you just can. Since you can use combat skills outside of combat you can, if you want, murder everyone in town. If you have a character that can turn invisible then they can do so and steal everything in a shop or house while the owner stands there oblivious. If you have a character that can talk to animals then some animals will give you quests because *of course* animals have things they want too (animals in general will offer lovely little bits of commentary on the locations they inhabit). The game doesn't tell you what you can and can't do and if you *should* be able to do it then generally you will be able to, but it's up to you to think of it. There's a remarkable degree of freedom for a modern RPG.

{% include img.html image="divinity_hatch.jpg" caption="My friend called his cyan-haired warrior 'Mr. Whippy'" %}

There's no abstract overworld map in Divinity; it's one contiguous world made up of very large, open spaces to explore. It's an as-good-as open-world game with very minimal gating — you're free to explore the current map in its entirety but you usually can't get to the next map without getting to a certain point in the main story. The maps really are rather large and any settlements, and the buildings within them, in a particular area are contiguous with their surrounding space rather than separate levels, which really helps to give each map a sense of being a specific place rather than just a backdrop. Only dungeons are separate levels and, like the main maps, they all feel distinct and interesting. There are no generic filler dungeons in this game, every single one feels carefully and thoughtfully designed to convey a very particular mood and distinct character.

The locations are familiar, certainly — a coastal trading port surrounded by plains and cliffs, a wild woodland, snowy mountains, churches, caves and so on — but a great deal of care was clearly taken in both the level and aesthetic design to make each one feel *right*; the snowy highlands feel *cold* and dangerous, they aren't just the plains painted white. It's a beautiful game — vibrant and rich without being cartoony — with an art style that renders sunny uplands as cogently as it does dingy occultist shrines while maintaining an absolute overall aesthetic cohesion.

It's worth noting that, within these large maps, you can split your party up and send each of them to different places to do different things. This is of particular value when playing co-op since you can get a great deal done quickly in towns by splitting up.

## You

In Rivelon there is a magic force called the source. The use of which is called 'sourcery'. I can't decide whether that's genius, idiocy or idiot-genius. At the beginning of the game you will create two characters who are both Source Hunters — an order dedicated to stamping out the use of sourcery by, in an irony that escapes no one including the main characters, using sourcery.

You can select from a number of pre-defined classes but there aren't *really* any classes in Divinity. Your characters have attributes, abilities, skills and perks. Attributes are the usual strength, dexterity, intelligence and so on which dictate how good you'll be at particular things. Abilities include weapon types, magic schools, talking, crafting, sneaking and the rest. Perks are perks, we all know what perks are. You get a set number of attribute, ability and perk points to spend at character creation and gain more periodically when you level up.

{% include img.html image="divinity_poison.jpg" caption="Zombies & pigs: natural allies" %}

You'll notice I missed out skills. This is where it gets interesting. Skills are learnt from skill books, which can be found as loot, bought from traders or crafted. What skills you can learn is dictated by your abilities and these can be mixed and matched freely. So, for example, say I have a character with a point in Aerotheurge (air magic) and one in Man-at-Arms (fighty stuff) then that character can learn, from skill books, 3 novice air magic skills and 3 novice mêlée fighting skills. More points in each would allow me to learn more and higher-level skills. Say that later on I decide it would be cool if this character could summon a spider — no problem, I just drop a point in Geomancy and I can learn 3 earth magic skills.

There are a *lot* of skills and they all tend to be genuinely interesting rather than slight variations on the same thing.

This skill system opens up a lot of strategic options. There are, really, no classes — any character can potentially do anything. So the most obvious and primary strategic decision is between being a generalist who can do a bit of everything or a specialist who is *very* good at one thing. There's a *huge* opportunity for synergies and finding complimentary skills is a lot of fun — particularly since skill book drops are somewhat random. It's not just about deciding what skills you want but about who does what — do you want one character to be responsible for all the summons or do you want your characters to have one summon each? Do you want a dedicated support character or to have four self-reliant characters? Do you want to build your character around one particular skill in the hope that it drops during this particular playthrough (it may not)? Do you want to spend this next ability point on weapon skills (making you more effective with a particular weapon type) or on opening up more or higher level skills? There are a lot of strategic possibilities before combat has even begun.

It's a very flexible system that really encourages experimentation. It's about finding out what works for your characters and how you want to play. It's an iterative process of continuous adjustments, refinements and redesigns as you find new skills or face new challenges. It also strikes a nice balance between commitment to a decision and freedom to experiment in that you can't un-spend ability points but you can unlearn skills to make way for trying out others.

The two main characters in my current playthrough are: Drewiepops, a Rogue-Witch who likes turning invisible, stabbing people in the back, leeching their life and generally cursing and hexing them. And Piento, an Air and Water Mage who also uses a claymore. She likes tricking people into thinking she's a mage so that she can claymore them in half when they get close.

{% include img.html image="divinity_party.jpg" caption="Piento eschews armour in favour of a lilac dressing gown" %}

You can pick up companions as you make your way through the game (up to a maximum party size of four) or, a little later on, design your own companions to fill a specific gap.

## Burn them all

The combat in Divinity is just lovely. The varied and flexible skills, interesting and diverse enemies, the tactical importance of positioning and use of terrain and the turn based nature would make for great combat on their own. What pushes things beyond great into exquisite is the elemental anarchy and that systemic approach I mentioned earlier.

This gets a little involved so I'll just spill it out:

A rain spell will cause, as you'd expect, rain. Rain will make puddles in dips in the land or, where they already exist, cause them to expand. Lighting will electrify puddles (of any conductive liquid — blood as well as water), potentially stunning anyone who stands or walks in them. Ice magic will freeze puddles, people who walk on ice are very likely to fall over. Fire will melt ice and cause water to turn to clouds of steam. Steam clouds can be electrified with lightning. Rain also causes anyone caught in it to become wet. Wet people are more susceptible to both freezing and stunning. Earth mages can place down oil which slows people who walk through it. Fire will cause oil to burn. Warm characters are more susceptible to burning. Water puts out fire. Poison, in both liquid or cloud form, will explode when hit with fire.

Enemies with an affinity for a particular element are often healed by that element's effects.

The elemental effects are not only for mages, there are elemental arrows and grenades (and water balloons) too. Barrels of various elemental liquids are also scattered around (oil, water and poison) and will burst quite easily when hit, whether intentionally or by accident.

You can move (if your character is strong or telekinetic enough) objects such as barrels, crates, bails and so on around in the world to, for example, create barriers in order to funnel enemies.

What this all results in, early in the game, is utter chaos. That meadow you started fighting in will look, after the battle, like a post-apocalyptic wasteland. What makes Divinity's combat great is learning to control the chaos and use it to your advantage. To work out a good way to approach a particular enemy with the tools at hand. To come up with and execute a *plan*.

{% include img.html image="divinity_firey.jpg" caption="This was a nature reserve before the fight began" %}

When I was playing co-op, large battles would usually be preceded by lengthy strategic discussion wherein we weighed up our various options and tried to select the best from all the many branching possibilities. It doesn't always go right, of course, it's complex enough that there'll often be something you didn't take into account or simply didn't know, but coping with that is a good part of the fun. It's about making a plan and then adjusting it on the fly as things change. Weighing up which character would be best doing which thing, how best to use the party's resources. It's extremely enjoyable in single player but in co-op it's outstanding.

Combat in party RPGs can sometimes be tedious. After a while you've learned what the effective skills are and how to position your party, and battles become routine — slight variations on tune. Divinity doesn't completely avoid this — I think it's an almost inevitable pitfall of the genre — but it puts it off until much later. For the vast majority of the game no two battles will play out quite alike as the enemies, terrain, your characters' abilities and the available elemental madness vary.

## High praise

I love Divinity. It's the most enjoyable RPG I've played since [Morrowind](/OpenMW/).

It's a big, beautiful game. I spent around 200 hours playing the pre-Enhanced version (in Wine). That represents one complete co-op playthrough and one almost complete single player game. I've been playing the Enhanced Edition for about 50 hours split fairly equally between separate single player and co-op games. From what I've seen so far the Enhanced Edition is an improvement on an already superb game, all of the additions and tweaks so far have been for the better.

{% include img.html image="divinity_gate.jpg" caption="I said it <em>looks</em> clear" %}

If you like playing with systems, exploring interesting locations in a self-directed way and strategic combat then I can't recommend this game highly enough, especially if you intend to play in co-op. If you play games like this mainly for the story and want a rich, deep narrative with a labyrinthine plot, reams of lore and character development that occurs explicitly in the game rather than in your imagination then you *may* want to look elsewhere. You'd still probably like it though, just not as much as [Pillars of Eternity](http://store.steampowered.com/app/291650/).

Divinity: Original Sin - Enhanced Edition: The Return of the Philosopher's Goblet of Fire - Part II: The Wrath of Khan is available on [Steam](http://store.steampowered.com/app/373420) and [GOG](https://www.gog.com/game/divinity_original_sin_enhanced_edition).
