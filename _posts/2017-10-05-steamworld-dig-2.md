---
title: "Steamworld Dig 2"
image: steamworld_dig_2_featured.jpg
subtitle: "*sparkle*"
---

Steamworld Dig was a gem of a game that felt like a nice relaxing holiday amongst all the rougelite action platformy games — it was nice to play an explorative platformer that didn't feel like it wanted to *punish* me at every turn. It was casual in all the right ways without being at all bland. It knew what it wanted to be and had the confidence to just be it.

Steamworld Dig 2 takes everything I loved in the first game and makes it better.

<!--more-->

Freed from the desire to punish the player, Steamworld Dig 2, like its predecessor, hands you a bunch of tools and a playground and says: Go and have fun! Try things out and see what happens! *Explore!*

{% include img.html image="steamworld_dig_2_town.jpg" %}

The game has a narrative and it's lovely, helping the game feel like a journey rather than *just* exploration of an environment — but it's not *why* you play. It's a sandboxy open-world feeling game about seeing what's round the next corner (and mining it).

You play as Dorothy, a steambot who's decided to go looking for her pal Rusty, the protagonist of the first game. This involves going down into mines, mining stuff, finding caves and exploring them, gaining new tools, going back to town and selling what you mined, upgrading your tools and going mining again, much as Rusty did. Everything about it is improved over the first game, though — the tools are better, the caves are puzzlier, the upgrade system is far more fleshed out and the game is less linear and a lot bigger.

On top of all of the improvements the few rough edges have been smoothed away, too. For example, health is now replenished when you go back to town rather than having to pay to top up, and filling your water tank no longer depletes water pools. Those things added nothing to the game so they were dropped. The devs really have shown uncommon good sense.

{% include img.html image="steamworld_dig_2_top.jpg" %}

Rather than one largely linear mine/cave we now have a series of interconnected mines/caves/areas which can pretty much be explored freely — the narrative guides the player but doesn't restrict them. You can explore as much as you like looking for secrets (of which there are *lots*) and trying to find every cog to upgrade your tools, or you can just go where the narrative wants you to go; it's entirely up to you.

The caves are great now. They're small self-contained levels within the broader level which usually take the form of a little puzzle. And they're *really good puzzles* — they're not just *press the switch to open the thing* — some of them really had me thinking hard. While in a puzzle cave you're free to try things out without risk of losing the resources you're carrying — dying just results in a reset at the start of the cave — which allows a proper crack at solving the puzzle without the anxiety of losing anything.

{% include img.html image="steamworld_dig_2_temple.jpg" %}

It's a relaxing, *fun* game to play. There's something interesting around every corner whether a new environment, a fun little puzzle, a challenging encounter or just resources to gather. It's easy to get lost in and end up playing for hours and hours. It's a delightful game that manages to be cute without being twee, funny without being silly, relaxing without being boring, casual while still challenging, and it feels free and open without being confusing or overwhelming.

Steamworld Dig 2 feels like the product of a very clear-sighted group of people who knew exactly what they wanted to make and made exactly that. It's the gem of Steamworld Dig polished until it gleams.

You can buy it in all the usual places: [gog](https://www.gog.com/game/steamworld_dig_2), [Humble](https://www.humblebundle.com/store/steamworld-dig-2) and [Steam](http://store.steampowered.com/app/571310/SteamWorld_Dig_2/).

Here's a minuteish of random gameplay.

{% include yt.html vidid="4iWvS7WzF-s" %}
