---
title: "Rocket League"
image: rocketleague_featured.jpg
subtitle: Kicky cars
---

Rocket League is about the joy of motion and the satisfaction of interacting with physical objects. It's instantly gratifying. Your first game will be *awful* — six players just chasing the ball, tripping over each other in a desperate scramble to slam their car into it — but it will be *fun*. It's the pleasure we get throwing a ball against a wall or balls of paper into a bin; the basic, animal joy of making something fly through the air.

<!--more-->

It is, mechanically, a very simple game; each player gets a cuboid and they have to propel a sphere through a rectangle. The fluency and adroitness with which you control your cuboid is the entirety of the game. What starts off as a clumsy, disorganised struggle to hammer your car into the ball becomes, with time, an exquisite ballet of aerial passes, air dribbles and off-the-wall freestyle goals.

And when I say "with time", I mean a *lot* of it. I'm ~1000 hours in and the skill ceiling is nowhere in sight.

A game of Rocket League feels like what we wish sports felt like — an idealised version of ball-sports. It's what sports *look* like they feel like in films or compilation videos of the "10 greatest shots/goals/hits/plays ever" — Rocket League feels like that *all* the time. It's all the excitement, drama and *fun* of sport without the filler, the down-time, the *boring bits*. It's that feeling of tracking a ball on its arc through the air and launching yourself at it at the exact right moment and angle to propel it into the goal boiled down to its essence.

It's the Platonic ideal of sport.

{% include img.html image="rocketleague_car.jpg" %}

The game has various modes; you can play online, cross-platform, with friends or strangers in ranked and unranked matches of up to eight players. The basic game is football in cars (or 'Soccar', as the game calls it), but there's also Hoops, which is more like basketball, Snow Day, which is akin to ice hockey and the new Rumble mode which is like Rocket League brought to you by Acme — you get various power-ups like a boxing glove on a spring or a plunger on a rope. 'Soccar' is likely where you'll spend most of your time, though.

Car customisation is a fun aspect of the game. There are various unlocks and drops which allow you to personalise your car to your liking with paints, decals, aerial ornaments, wheels and, of course, hats. What's the point of a car without a hat.

The car models themselves — some of which are available as DLC but there are plenty in the base game to keep you happy — all have the same basic abilities; the same top speed and jump height and so on. They do have different hit-boxes though — some are taller, some are flatter, some are wider and so on. This has the side-effect, due to the game being so heavily physics-based, of influencing the turning circles of the cars — some will turn more tightly than others and thus feel slightly different to play with.

{% include img.html image="rocketleague_gola.jpg" %}

The Linux port, which was a *long* time coming, is described as being in beta and has some known issues. It's prone to the occasional crash (though I've had none in my ~10 hours since the Linux port dropped) and, at least on my proprietary Nvidia drivers, lacks anti-aliasing. Otherwise the port seems fine, I can't compare to Windows as I've never run the game in Windows but it certainly compares favourably to Wine.

{% include img.html image="rocketleague_waste.jpg" %}

Rocket League is a perfect game — it gets what it is trying to do *exactly* right. It's joyous to play and manages to be fun at *all* skill levels (the game does a decent job of matching you with players of similar experience). It requires no knowledge going in — everyone knows how to hit a thing against another thing and make it fly — but provides an arena for you to learn, improve and hone your skills over thousands of hours if you so desire.

It's pure, distilled, concentrated fun. 

Rocket League is available for Linux from [Steam](http://store.steampowered.com/app/252950/) and [Humble Bundle](https://www.humblebundle.com/store/rocket-league).

To help get your dander up, here are the pros from the final of the recent [RLCS](https://www.rocketleagueesports.com/) tournament showing us what can be done with a flying car:

{% include yt.html vidid="9yC6HD39sZQ" %}

