---
title: "Hyper Light Drifter"
image: hyper_light_drifter_featured.jpg
subtitle: Dodging off ledges  
author: uoou 
---

There are lots of pretty looking indie games around but, often, the beauty is superficial. Pretty pictures on top of what is barely a game.

Hyper Light Drifter is beautiful *all the way down*. Its gorgeous aesthetics are married with intelligent, inventive game design choices throughout. Each aspect of the game's design speaks in unison with all others — this isn't a jam session, it's a symphony.

<!--more-->

Speaking of music, the game's soundtrack is, without hyperbole, *perfect*. I usually end up turning music off in games; even when I like it it begins to grate through repeition. The music in Hyper Light Drifter, like everything else in the game, just fits so perfectly — *is so much a part of the environment* — that I wouldn't dream of disabling it.

{% include img.html image="hyper_light_drifter_ledge.jpg" %}

Environment is very important, the world of Hyper Light Drifter feels like a place with history and culture. It's mysterious, as an alien culture should be, and exposition is understated to say the least — nothing is spelled out, the only text I've encountered has been in the UI — it just exists, out there, for you to explore, unravel and decipher. The game communicates what it wants you to do through silent vignettes and a symbolic language; it *shows* you, it doesn't tell. It's up to you to work out the semiotics.

This attention to communication flows through the game — you're never told too much, just enough that you can work it out if you choose to, but all the information is there. Locations and enemies are readable, you get a sense of what might be a hazard, how enemies might act, where a secret might be and so on from visual and kinetic clues. But you're expected to do the work to piece it together, you are not led around by the hand.

Hyper Light Drifter *wants* you to explore, to get lost, hit dead ends and work out how to get past them. As with the best of RPGs I always have a task in mind, then get distracted by something intriguing *over there*. And then get distracted by something else and then struggle to recall what it was I was actually trying to accomplish an hour or so ago. I've not finished the game yet — I want to take my time and explore every single corner of this world.

{% include img.html image="hyper_light_drifter_town.jpg" %}

Combat is the meat of the game and it's frustrating and satisfying in that perfect mix. Fights are hectic and kinetic but — importantly — controlled, it's when you lose that control — mis-timing a hit, messing up your chain-dodge, mis-reading an enemy, dodging over a ledge — that you will die. It's very unforgiving, punishing even, but never unfair; if you fight *well*, you will win. Combat is about watching your enemies, learning how they move and when they hit and then dodging and hitting them accordingly. It's challenging and rewarding in that way we all remember from older games, relying on skill and excellent controls, rather than protecting players from any failure as more modern games too often do — it's hard to feel satisfaction when you can't really lose.

Your tools in combat are your sword and your dodge (and your wits). You also have a pistol which fills with bullets as you hit stuff and, later, grenades. These weapons can all be upgraded in various ways as you progress through the game — you can upgrade your dodge such that you can, with precision timing, chain dodges together, for example — but your tool set remains simple and elegant. You don't get more powerful through upping the damage numbers on your weapons, you do so by learning to use them effectively.

I'm reluctant to talk too much about this game, it's so much about the joy of exploration and learning the game's languages and mechanics that too much description would only detract from the experience should you choose to play it.

{% include img.html image="hyper_light_drifter_cute.jpg" %}

This is a beautiful, thoughtful game which does not compromise the integrity of its design at any point, resulting in a sprawling but coherent world, begging to be explored and deciphered, and offering challenge and reward in that perfect mix at all levels. The *only* caveat to my otherwise unreserved recommendation is that the game is capped at 30fps, and I know that really bothers some people. I would've preferred it to run at 60 because why not but, due to its art style and animation, I honestly barely noticed.

Hyper Light Drifter is available on [Steam](http://store.steampowered.com/app/257850/), [Humble](https://www.humblebundle.com/store/hyper-light-drifter), [GOG](https://www.gog.com/game/hyper_light_drifter) and the [Dev's site](http://www.heart-machine.com/).

Here is my pal Hex using his extraordinary skills to somehow show us this game through some sort of moving pictures:

{% include yt.html vidid="OWcIewhCeDo" %}



