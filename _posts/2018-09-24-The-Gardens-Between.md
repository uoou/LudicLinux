---
title: "The Gardens Between"
image: gardens_between_featured.jpg
subtitle: "Re-re-wind"
---

The Gardens Between is a beautiful time-manipulation puzzle game interwoven with a story of friendship and childhood memory. We follow two friends as they traverse puzzley levels filled with and constructed from their memories of play and shared adventure. 

<!--more-->

Each level in The Gardens Between is an island, and each island tells the story of a day spent playing together or a shared adventure. The storytelling is woven into the fabric of the game — there is no spoken or written word — the mundane objects of childhood play are transformed into terrain, obstacles and instruments of the level's puzzling while the alchemy of the characters' play weaves them into wondrous adventures. 

As we follow the two friends through their shared memories we learn about each of their characters through details of animation, gestures and mannerisms. One is headstrong and bold — usually taking the lead and traversing treacherous terrain without pause, her head held high and always looking ahead. The second is cautious and careful — hesitating until he's sure it's safe, walking with timid steps, staring at his feet as he places them. Without any dialogue at all, we *get* these two young companions and understand why and how their friendship works.

{% include img.html image="gardens_between_light.jpg" %}

The objective on each island is to get an orb of light to the end of the level. Controls are very simple — you can make time go backwards and forwards and you can interact with certain objects in the levels. The puzzle is getting the orb into the right place past various obstacles which often involves a bit of creative thinking and some trial and error. As you zip back and forth in time trying to work out exactly what goes where, when, and the camera swoops sinuously around the island, you can't help but appreciate how beautifully crafted these levels are. They have a lovely flow and dynamism to them, with animations adding charm while also being mechanically intrinsic. The game does a wonderful job of bringing you into this world of children's play and memory and making it a real, dynamic narrative and ludic space.

As you progress through the game the islands get weirder and darker which lends a sense of time's passing and a day of play melting into evening. Bright certainty giving way to the mysteries of the dark and play's end.

The puzzles themselves are fairly simple — they're more about careful observation and looking for subtle clues than coming up with lateral-thinking craziness. As puzzle games go it's on the more benign side, its cleverness lying more in the fusion of narrative, puzzle and environment than in the mechanics of the puzzles themselves. It's a gentle experience. It certainly wants to make you *think*, but it wants to delight you more than it wants to stump you.

{% include img.html image="gardens_between_dark.jpg" %}

I thoroughly enjoyed my time spent with The Gardens Between, it tells a small story very cleverly, thoughtfully and beautifully.

It made me cry a little.

I should point out that it's a short game — it took me about 3 hours to arrive at the ending — if that bothers you, well, you'll know. It was so lovely, though, that I'll certainly be playing through again (and looking for <span class="jiggle">hidden things</span>).

The Gardens Between is available for Linux on [Steam](https://store.steampowered.com/app/600990/The_Gardens_Between/).












