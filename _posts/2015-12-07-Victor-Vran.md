---
title: "Victor Vran"
image: vran_featured.jpg
subtitle: Why did it have to be <del>snakes</del> spiders?
author: uoou 
---

[Victor Vran](http://store.steampowered.com/app/345180/) is an ARPG (the devs have even added "ARPG" to the game's name on Steam, so you can be sure). As such you will be killing a *lot* of spiders and skeletons (some of whom will be wearing bowler hats). This game has so many spiders that it pushed the average leg-count of enemies up to nine. One cave in particular housed such a huge number of spiders that it crashed my co-op partner's game (this was during early access, don't worry).

To best highlight some of the things Victor Vran does right, I need to outline a few things ARPGs often do wrong...

<!--more-->

Torchlight was a great game. The skills were fun (they were varied, some were pleasantly overpowered, some were just satisfying) and there were plenty of enemies to kill. That's all I really ask for in an ARPG; satisfying levelling and lots of enemies. ARPGs are, after all, genocide simulators — you go to creatures' homes and murder them. *All* of them. The only real problem with Torchlight (ok it was a bit easy but that's fine — I could stick on a few podcasts and commit relaxed genocide) was that it wasn't co-op.

Needless to say, I was really looking forward to Torchlight II. A few hours into it though, my friend and I were bored. They'd improved everything about the game *except* for the skills and number of enemies. They'd *balanced* the skills. They were dull, samey and, worst of all, I had to pump skill points into skills I *didn't* want in order to get to the ones I *did* want, making levelling utterly unsatisfying.

The story was great, I'm sure. Probably. If you think I'm playing an ARPG for the story then you're sorely misunderstanding my approach to the genre.

Victor Vran is in many ways the game I wanted Torchlight II to be.

## Story?!

In a game like this I am soldier and Victor Vran understands this. I don't need explanations or reasons, I just need orders. If I'm in a room full of skeletons *I know what to do*, I don't need a *reason* to massacre skeletons. It's who I am.

The story in Victor Vran — something about murdering lots of spiders and skeletons to support the monarchy — is by no means bad; the bits of it I actually paid attention to were nicely written and rather compelling. But the developers clearly understand that the story in such a game is not my motivation. It is context; it is a description of the state of the world, not my reason for fixing it (by killing lots of things).

With the story out of the way, we can focus on what matters in a game like this: skills and combat. In a shocking departure from the ARPG formula Victor Vran allows you to jump! (you can even wall-jump!) Less shockingly, skills and classes as we know them are gone.

## What these games are *really* about

Victor Vran takes a leaf from Guild Wars 2 in that your skills are determined by the weapon you are using, each weapon having two activated skills. With a hammer you can do a slam, a scythe allows you to whirlwind, a rapier lets you dash and so on (there are also swords, mortars, shotguns and lightning guns).

Your choice of outfit (you're offered a selection at the start and pick more up as the game goes on) is how you paint the broad strokes of your character; how warriory, roguey or magey they will be (there are also some hybrid options). Your outfit dictates, in a very broad sense, your class (much like in real life).

{% include img.html image="victor_vran_fight.jpg" caption="They had it coming" %}

As you progress through the game you pick up cards and unlock card slots on your character. Cards convey bonuses or passive skills which you can use to focus and fine-tune your character to your liking. This is where you tailor your character to your specific play-style. Do you want to focus on crit chance? Crit damage? Raw damage? Defence? Do you want an explosion that fires off when you crit or when you overkill? There are *lots* of cards and their effects are varied and sometimes pleasantly weird.

Cards have levels and your character has a number of destiny points, which increases as the game progresses, and which limits the total combined level of cards you can equip. For example: if you have 4 destiny points and 2 card slots then you can equip 2 level 2 cards, one level 4 card or a 1 and a 3. Straightforward. Except that there are cards that become more powerful the more cards you have equipped. There are other cards which raise your destiny points but do nothing else. It's a complex and nuanced enough system to more than make up for the lack of a skill tree. Finding a new card — one you've not seen before — always leads to a minute or two of careful consideration as to how that card could best be used, what play style it would support.

{% include img.html image="victor_vran_cards.jpg" caption="Plotting murder" %}

I was worried, since you can swap out your outfit, weapons and cards at any time, that there may be a lack of permanence; that the game might be missing that sense of having made a choice and sticking with it, making it work. That's not the case though — they've balanced it right, they've just prevented the regret of choosing a skill then being disappointed with it. Changing out a single card won't have a huge impact since you will have selected them to synergise, you'd have to replace them all in order to fundamentally affect the way your character plays. And that would mean finding another set that work well together.

Your choice of weapons (you carry two, which you can freely switch between) determines your play style to a large extent - the rapier is inherently mobile, for example, good for getting out of trouble and picking a single target to hit very hard, whereas the hammer is better for dealing with large groups of weak enemies and requires being stationary while attacking. You're not limited by this though, if you *want* to play a tanky rapier-wielder then you can, you just need to choose the right outfit and find the right weapon and cards (or craft them, there is a crafting system in the game which encompasses all gear).

There are also powers, equippable items (you start out with one power slot) that you gain as you play. Powers do things like rain down meteors on enemies, provide a defensive bubble or generate an aura that improves allies' attack speed and power. Powers are fueled by overdrive, which is *sort of* mana, except that how you get it depends (largely) on your outfit. Some give you overdrive when you're hit, others when you hit or crit, others provide it at a regular rate. 

This all adds up to a hugely flexible system which allows you to get *very* specific about how you want to play, and the crafting system smooths out the luck aspect of random drops, allowing you to craft that one card you *really need* rather than having to wait for the dice to be kind. This really comes into its own in co-op where, rather than all just bashing on the enemies, you'll find yourself taking on a very specific role. The big hammer-wielder can take on the mob and protect you while you perforate that one tough fella with your rapier. Or you can all act as diversions until your friend, who's gone big into overdrive generation, gets enough to fire off their big power.

Combat is based on player dexterity far more than in similar games and positioning is vital. How you need to think about positioning varies depending on the weapon you are using; with the shotgun or rapier you'll be moving around the outsides, picking your targets and avoiding damage. Whereas with the scythe or hammer you'll want to be right in the fray, trying to hit multiple enemies at once and dodging around for advantage. Combat is very active and kinetic, there's no just standing there and whaling on enemies in Victor Vran. If you want to live, you'll constantly be on the move. Large fights, of which there are plenty, have a frenetic intensity that games like this seldom manage.

## The places where you kill stuff

The game takes place over a series of maps accessed from a central hub. Each map has a (decent) number of dungeons to explore and you can, within a given map, do this at will in a non-linear way or just go to the next main story dungeon and ignore the rest — it's up to you.

Each dungeon and map has a series of optional challenges to perform — find all secrets or kill 70 skeletons within 2 minutes, that kind of thing — the completion of which rewards the player with loot, XP or money. Some of these challenges are *really hard* and resulted in a lot of strategic discussion amongst my co-op partners and I. If you complete *all* of the challenges in the game, you unlock a second level of *even harder challenges*. If you enjoy co-op games where there is actual co-operation — where it's not just a case of doubling the numbers but each person taking on a specific role and working with and around the others — then these challenges are a *lot* of fun. Again, the devs are exhibiting that they understand why I, at least, play games like this.

{% include img.html image="victor_vran_map.jpg" caption="Satellite imagery of enemy locations" %}

Locations in the game are varied, expansive and usually rather pretty. Abandoned Victoriany streets and rooftops (I hit one skeleton so hard he landed on the next roof over), the crypts and churches you'd expect, manor houses, open countryside, warehouses, caves and some sort of not-quite-hell other dimension of floating islands. Enemies are equally varied, both in appearance and in terms of how you need to approach them, but tended to be a bit more area-specifc than I'd like. If you're in a town you're going to be fighting a *lot* of gargoyles, in a cave it's going to be pretty much all spiders and so on; but that's really a failing in ARPGs generally. There are different types of spiders, gargoyles and everything else, it's never a case of fighting the exact same enemy type for an entire dungeon by any means, and the different types are genuinely different — it's not just cosmetic, they each need approaching differently. 

{% include img.html image="victor_vran_skeletons.jpg" caption="A target rich environment" %}

(There are also party skeletons. They're surrounded by disco lights and... they just dance. If you get too close, you dance too. You can't fight while you're dancing, not in this game at least. Absurd enough to be funny, especially during a hectic fight)

## Conclusionicide

The devs really seem to care about this game, it feels like they made the game *they* wanted to play. They listen to the community (perhaps a little *too* much) and there are [regular updates](http://steamcommunity.com/games/345180/announcements/) including 'free DLCs' (things like daily dungeons and new weapon types).

It's not a perfect game by any means but it does a *lot* more right than it does wrong and, because it does all of the *important* things right, it really scratches that ARPG/dungeon crawler/hack 'n slash genocidal itch.

It's a superb co-op game to play with friends and one that often requires *actual co-operation* and teamwork rather than just a doubling of firepower.

The game plays perfectly well on both controller and keyboard and mouse. I would not normally play a game like this on controller but in this instance it felt better to me. My co-op partner played with mouse and keyboard and was equally happy.

Oh, and there are lightsabres in the game. I've not found one yet but I've seen them in videos. They even make the lightsabre noise when swung, \*VZzzeeeeiou\*. I really want one.

This game deserves a lot more attention than I've seen it getting. I think the lack of classes and a skill tree might be giving some people the impression that this game is more A than RPG. While it certainly has plenty of A, and the combat is far more about player skill than other ARPGs, I'd argue that it's no less RPG for it. The weapons/cards/outfit/powers combinations have at least as much depth and flexibility as the class and skill based systems in other games and, since you can usually respec in those games anyway, there's no less permanence either — it'll take a *long* time to find and refine that perfect combination of cards and it's not something you're going to discard on a whim. This game is firmly ARPG, not an action game with light RPG elements as some may have been led to believe. Moreover, combat is actually *more* thoughtful and strategic, as well as being kinetic and intense, than in other such games.

Victor Vran is available on [Steam](http://store.steampowered.com/app/345180) and [GOG](http://www.gog.com/game/victor_vran). Maybe look out for it in the Winter sales!
