---
title: "The Dwarves"
image: dwarves_featured.jpg
subtitle: "Oink, Oink"
---

The Dwarves is a narrative RPG with interesting real-time pauseable combat mechanics. The game's marketing has tried really hard to sell this as an all-out action RPG and I think that's a shame since it's really not. And what it actually *is* is far more interesting than that.

<!--more-->

**I've not read the novel this game is based on** and have no particular interest in doing so. I'm not going to be talking about how the game relates to the book since I neither know nor care. I'm judging it as a game in its own right.

With that out of the way...


The Dwarves has three distinct modes of play — the overworld map, which plays like a choose-your-own-adventure as you move from location to location and are presented with textual vignettes as you meet people and encounter settlements; combat levels when you meet enemies or decide to attack someone as a result of your choices and exploration sections, presented in the same way as the combat levels, but most resembling a point and click adventure game where you must look for clues to solve a puzzle to open a secret door or reveal a treasure.

{% include img.html image="dwarves_map.jpg" %}

The blending of these three modes really successfully evokes the feeling of playing through a storybook — not *reading* a story, it's distinctly active, you *are* the protagonist and your agency is profound — the different paces suggest the cadence and rhythm of a written story, with the travel sections being more suggestive than explicit, the exploration and adventure passages being more detailed, personal and affective and combat being hectic and precarious while still having a strategic feel.

The battles tend to be set-pieces — a single map where you have a specific objective like protect so many villagers, kill all the enemies or get to the exit without dying. Combat is *very* physicsy with many skills involving throwing or pushing enemies around to manage them. When combat starts you can take any four of your party members into the battle and they can each have any four of their unlocked skills active. The key to combat is to choose the right party members and skills for the particular battle and to use them together effectively — lots of pausing, issuing orders to multiple characters and then unpausing to see how your plan plays out. It's *very* satisfying — there have been several occasions where my plan just clearly was not good and I had to stop and have a re-think about my approach, opening up character sheets and looking at skills, trying to spot synergies I'd overlooked.

{% include img.html image="dwarves_fight.jpg" %}

Many of the combat levels have obstacles like pits or fires that enemies can be pushed into for an instant kill and using your team's skills to push enemies around into just the perfect position so that one of your characters can just sweep them into a chasm is *hugely* satisfying. Fights have a lovely immediacy to them, they're violent and brutal in a satisfyingly chunky way — very dwarfish.

When not told to use a specific skill your party members won't just stand there like fucking idiots, oblivious to the turmoil around them, they'll stay roughly in position but will use their basic attack on nearby enemies. You don't have to *tell* them to not just stand there while getting whaled on by orcs, which is a real blessing. Affordances and simplifications like this (you can't, as far as I've found, queue up orders, for example) prevent combat from feeling like micro-management; this isn't about giving a load of orders from an aloof position, you feel like you're *in* the battle, shouting orders to your friends, trying things out and seeing the enemy react, and then reacting to their reaction. Battles are immediate and intense while also feeling strategically satisfying — a combination I can't recall experiencing in another game.

Mechanical character development is pretty shallow. There are no weapons or armour to equip and the skill trees are basically linear, with characters just getting the next skill when they level up, with the occasional choice between two. I don't actually mind this and it's certainly better than having lots of almost-the-same skills to chose from and that feeling of grinding XP to get to the skill you really want. The characters each have specific personalities and their skills are specific to that role. This isn't a game about having a vast number of tools, it's about having a decent number of well-differentiated tools and using them well together. 

The story is lovely, that sounds lazy but it's the right word. The main character, Tungdil, is an earnest, sweet young dwarf who was raised by humans and dreams of seeing the world. Guess whether he gets to? This is proper high fantasy at its uncomplicated, heroic best. Which for me makes a refreshing change from all the *oh so dark and broody* troubled anti-heroes that plague everything. Uncomplicated is not to say unsophisticated, the moral ambiguity, tough choices and darkness are all in there, it's just not thrust in your face every five seconds in a desperate clamouring to prove how edgy it is. It's proudly high fantasy with noble, virtuous characters to *balance* the troubled brooders, and I very much like that.

{% include img.html image="dwarves_home.jpg" %}

The writing is beautiful and its literary heritage is plain, I was hooked within the first few minutes as Tungdil's home and its inhabitants were described to me, as I explored, with artful efficiency — short, evocative passages that made me feel like I knew this person and his home in no time at all. The voice acting is generally good — some great, some merely ok — but the narrator is *perfect*; warmly lulling you into becoming a part of this world.

This quality continues through to the choose-your-own adventure style events and places in the overworld map. It's all beautifully described and encounters are engaging and seemingly quite open-ended. This is a very focussed narrative game which has a good story to tell and it does so beautifully, but without robbing the player of agency, choice and freedom. While largely linear, it very much feels like *your* story.

The weakest part of the game is the party management. As you adventure, you will find or buy various artefacts and objects that, when equipped, give buffs. The interface for assigning these to particular characters is vague at best — I actually have no idea who's got what and, since characters don't have an individual inventory distinct from the party's inventory, I have no way to check. Not far into the game I just gave up trying to give particular people particular things. It's not a huge deal as I've only encountered a few of these objects but it's a shame to lose this avenue of further specialising your characters. It's also impossible to easily compare two characters when considering them for the upcoming battle — there's no way to see two side-by-side — to assign them to the current battle you tick little boxes on their faces which is, again, vague. The whole party management UI is atrocious and I dearly hope it sees a complete re-design in a patch, I want to see, at a glance, who's assigned to the battle-party and be able to drop people in and out of that, compare them, give them stuff and so on. It's not the end of the world since you don't interact with this interface a huge amount, but it's a significant kink in an otherwise well designed game.

{% include img.html image="dwarves_ui.jpg" %}

Thankfully the combat UI, with which you'll interact far more frequently, has no such shortcomings and is perfectly efficient. The camera can act a bit stupid when it gets close to things like walls but that's an annoyance rather than a real hindrance and should be easy to fix in a patch.

The game currently has no vsync option but the devs say they're working on that. If that's a deal-breaker for you then wait for a patch.

The Dwarves is equal parts choose-your-own-adventure and quite innovative strategic action-combat with a bit of point-and-click adventure thrown in. Amazingly, these three parts sit well together and feel like a cohesive whole whose purpose is to bring a wonderfully engaging epic fantasy story to life. It feels like a seamless experience with a varied tempo and tone rather than a collection of disparate parts. It's an unusual structure but it works perfectly and I'd love to see the developers refine and expand it further.

The combat in particular shines — not because it's better than the others parts, they're all good, but because it manages to feel both actiony *and* strategic. It's exciting and intense but will also have you pausing and thinking hard. It's simple in all the *right* places — there's no fiddly movement, micro-managing actions or choosing from a hundred basically identical skills — to free the player up so that they can really think about how to use their party members, with their distinct skills and personalities, to best effect. It's a *lot* of fun.

{% include img.html image="dwarves_adventure.jpg" %}

My only reservation in terms of recommending this game is its price. *I've* thoroughly enjoyed every minute with it and I feel like it's *well* worth the cost. But £30ish is a lot of money for a game. If you like the sound of it and the price seems fair to you then I'd say go for it — it's an excellent, interesting realisation of what a narrative action RPG can be. If you're unsure, then perhaps hold off for a sale.

The Dwarves is available on [Steam](http://store.steampowered.com/app/403970/), [Humble](https://www.humblebundle.com/store/the-dwarves) and [GOG](https://www.gog.com/game/dwarves_the).



