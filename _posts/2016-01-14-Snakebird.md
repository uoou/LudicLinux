---
title: "Snakebird"
image: snakebird_featured.jpg
subtitle: Has genetic science gone too far?
author: uoou 
---

[Snakebird](http://store.steampowered.com/app/357300/) is an absurdly cute abstract puzzle game. It's one of the hardest puzzle games I've ever played, second only to the how-did-a-human-being-make-this-thing [SpaceChem](http://store.steampowered.com/app/92800/). Snakebird's difficulty doesn't derive from complexity — all you can do is move one of up to three snakebirds in one of four directions — it's about abstract logic and creative thinking.

And it really is preposterously cute. Just look at this little fella:

![](http://steamcommunity-a.akamaihd.net/economy/image/U8721VM9p9C2v1o6cKJ4qEnGqnE7IoTQgZI-VTdwyTBeimAcIoxXpgK8bPeslY9pPJIvB5IWW2-452kaM8heLSRgleGAo7JKwe94PvV92OD5C1Qm6uNPV2S2SRHWgmWQKrr7lwU0M8AnIBumkZ4a6N1SXp9jVuLgnHhZBg/62fx62f)

<!--more-->

One could easily mistake the playful aesthetic style as an indication that this is a mobiley, casual game. It's very much not. The gorgeous artwork, adorable animations (many background elements have sweet little animations when you click on them) and cheerful music belie what is an out and out hardcore abstract puzzle game. They also help dissipate the frustration of being stuck (and I spend a lot of time stuck in this game) — you just can't be angry at something *this cute*. When you dead-end your snakebird up against some spikes it adopts an expression of sheer worry, you can't help but be amused.

{% include img.html image="snakebird_sea.jpg" caption="When you don't remember last night" %}

Snakebird is a pure abstract logic puzzle game. Nothing is hidden, you could solve each level on paper if you so desired. Your objective is to get up to three snakebirds (it varies per level) out of the level by entering a portal. If there is fruit on the level then you must eat it all before entering, doing so makes your snakebird grow in length by one tile.

There are no time constraints or moving obstacles, snakebirds move one tile at a time, in the four cardinal directions, when you choose to move them using either keyboard, mouse or controller — I found keyboard the most comfortable; the key bindings are excellent. Snakebirds move like the snakes in the [Nokia snake game](https://en.wikipedia.org/wiki/Snake_%28video_game%29) with each of their sections following into the position of the previous one when they move, though in a turn based rather than continuous manner. Snakebirds are subject to gravity but if any part of them is resting on a surface (including another snakebird) they won't fall (unless that surface moves).

{% include img.html image="snakebird_sky.jpg" caption="Blue snakebird imperiously surveys its domain" %}

Much of the difficulty arises from what you fail to account for; not noticing that you'll be dead-ended, not noticing that green snakebird will fall when red snakebird moves and so on. The rest is due to the sheer difficulty of the levels — *how* do I get two snakes over *so many* spikes? Some of the levels seem impossible, you'll *know* you're missing something but you just can't figure out what. And then it clicks and you feel like an idiot and a genius all at the same time. It's the kind of game you need to go away and think about when you're stuck; the solution will often come to you when you're doing something else entirely.

(I was stuck on one level for *literally months*. I eventually got it. And then I was stuck on the next level. After a few hours of brain racking I decided to restart the whole game to give me a run-up. I will finish this game one day. I will.)

You're free to experiment in Snakebird as there's an unlimited undo function. This isn't a game about timing or getting everything right in one go, it's about *thinking* and nothing else. About using the tools you have to solve the problem in front of you with logic, creativity and lateral thinking. You're free to experiment; the undo function allows you to work towards the solution one step at a time, you don't have to solve the whole thing in one go. It's logic puzzling at its purest.

Later levels introduce new mechanics such as objects that you need to push around the levels with your snakebirds in order to climb on them to reach objectives. These aren't gimmicks — they don't introduce new rules or tools — they're modifications to or new things to do with the tools you already have. They expand your possibilities, and thus add difficulty, without complicating the rules.

{% include img.html image="snakebird_map.jpg" caption="Super Snakio World" %}

Like the best puzzle games Snakebird makes you feel stupid at times and smart at others. Sometimes both at once. If you like abstract puzzle games then, well, this is one of the best I've played and it's wrapped in beautiful aesthetics. Even the world map is adorable.

There's nothing bad to say about this game. It does exactly what it sets out to and does so with beauty, restraint and taste. Level design is exquisite, I never encountered a level that felt like a re-hash of the problems presented in a previous level — each one felt unique and was a perfect statement of a particular problem. Also it's *really* **really** ***cute***.

If I rated games numerically this game would score 3 out of 1. But I don't.

Snakebird is available on [Steam](http://store.steampowered.com/app/357300/) and [Itch.io](http://noumenongames.itch.io/snakebird).

