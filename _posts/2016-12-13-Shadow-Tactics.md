---
title: "Shadow Tactics: Blades of the Shogun"
image: shadow_tactics_featured.jpg
subtitle: "Footprints. Little traitors."
---

Shadow Tactics: Blades of the Shogun is a real-time tactics game in the vein of Commandos and Desperados, a genre that's really gone neglected since the early 00s.

<!--more-->

If you're unfamiliar with those games, the gist is that you infiltrate enemy territory with a handful of characters, making use of their special abilities to progress through expansive levels which are presented in a three-quarters top-down view. Success is about using these characters together in real-time to overcome or bypass obstacles in order to reach objectives. 

Playing shadow Tactics feels like a happy mélange of puzzle game, tactical RPG and action game. Most of your time will be spent carefully considering how best to position your characters and use their skills — looking at the level and the tools you have, going "Hmmm" a lot and coming up with a solution that you like. Sometimes you'll be reacting to something unexpected — an overlooked guard spotting you, a patrol noticing your footprints in the snow or an enemy hearing something you thought they wouldn't — the instances when you manage to turn a surprise to your advantage are hugely satisfying. For the most part, though, the game rewards cautious, thoughtful creativity.

{% include img.html image="shadow_tactics_woods.jpg" %}

The player characters — Hayato, a mercenary ninja; jolly samurai Mugen; cackling old sniper Takuma; excitable urchin Yuki and Aiko, savvy courtier and spy — are, unlike those in Commandos and Desperados, well characterised and likeable. Their interplay during missions and cutscenes is *just right*, deepening my affection for them, never becoming tired or grating. Yuki's humming to herself when she sets her spring trap is utterly adorable.

{% include yt.html vidid="Z-tu2G3qQqQ" %}

Shadow Tactics is nicely grounded in its historical Japanese setting. I'm certainly no expert on Japan's feudal period but it feels like the developers approached the setting with respect without letting it dominate. The option to have dialogue be in Japanese (with, in my case, English subtitles) is welcome — if these characters are Japanese then I want them speaking Japanese.

Levels are sprawling, *beautifully* crafted and truly open-ended — there's a goal or series of goals to accomplish, but how you go about that is entirely up to you and there are *many* ways to proceed. I've been playing a lot of roguelikes and it's nice to return to something with hand-crafted levels, particularly of this quality. They feel like a return to a time when players were given many options, lots of tools and a huge space to play in and trusted to decide what to do themselves — given the freedom to get stuck or confused and to think their way out of it, enjoying the satisfaction of having done so.

{% include img.html image="shadow_tactics_snow.jpg" %}

Another welcome anachronism is the quicksave. Flying contrary to recent game design trends, Shadow Tactics is *brazen* about its reliance on quicksaves. There's even a little timer up at the top which pops up when it's been a minute since your last save and becomes increasingly alarming in shade the longer it's been (easily disabled in the options if you don't like that sort of thing). The game is saying: Play around! Try things out! Experiment! Learn this game at your own pace and don't be afraid to innovate! Have *fun*! Quicksaves are a mechanic like any other. Sometimes abused, sure, but not inherently bad. This game is not ashamed of its quicksaving and I love it for that.

Successfully navigating missions hinges on using your characters together — using Aiko to distract a guard while Yuki lures another into her trap just as Hayato distracts a fourth and kills the one Aiko distracted in the first place. Observing guards' patrol routes and checking their view cones, looking for blind-spots and openings, is essential. Each character has a distraction skill, the ability to kill up-close and a third skill such as Yuki's trap and Hayato's shuriken. The distraction skills all do the same *sort* of thing, but they work differently enough that they offer real choices when approaching a situation — Yuki's flute lures a guard over to investigate whereas Hayato's thrown stone just causes guards to look in a particular direction and Aiko can pin guards in position, looking in a particular direction, if she converses with them while disguised. There are *many* ways to deal with a particular network of guards with their overlapping vision cones and choosing amongst potential solutions is often about style — not just solving the puzzle but doing so in an aesthetically and ludicly satisfying way — not just winning, but winning with *panache*.

To aid you in finessing things up, *shadow mode* is a tool which allows you to give each character one action to perform and then, when you press enter, have them perform those actions simultaneously. Executing a complex plan in this fashion and watching your little people carry it out successfully is *hugely* rewarding. 

{% include img.html image="shadow_tactics_cone.jpg" %}

Levels are huge and varied; from highland passes to castles to mountaintop temples. Some have mechanical twists, like a snowy town where player characters will leave dissipating footprints which guards will follow when spotted. Or a village at night-time with reduced vision cones and light sources which can be extinguished to allow a character to sneak past or to act as a distraction or lure — guards will attempt to re-light extinguished torches if they notice they've gone out.

AI is refreshingly sensible. Not smart, as such — AI can't be too smart in a stealth game without ruining things — but sensible enough for their behaviour to make sense and be predictable. Guards' view cones have a solid area and a hatched area, they will see you in the solid area whether standing or crouching but they will only see you in the hatched area when standing. Areas which are, from a guard's perspective, behind waist-high cover will always be hatched which makes low cover *extremely* useful for sneaking around. Being spotted is not instantaneous, when you enter a view cone the guard will become alert and the cone will gradually change colour in your direction — if you get out of the view cone before the second colour reaches you, you remain unseen. When guards think they've seen or heard something they will search around the suspicious area, checking bushes and other likely hiding spots before returning to their regular position. If you are fully detected then an alarm is raised and additional guards will emerge from buildings, making the entire level tougher. A nice approach that bypasses the "I guess it was nothing after all" problem that some stealth games suffer from. A non-lethal approach is possible but knocked-out guards will only stay that way for a very short time, so it's *significantly* harder, as it really should be.

{% include img.html image="shadow_tactics_tree.jpg" %}

Aside from the different challenges of static and patrolling guards, there are guards with straw hats who are harder to distract and also samurai which only Mugen can take down alone — other characters have to work in concert to overcome them. The quite significant verticality of the levels adds further challenge to stealthing your way around — you have to be very careful about being seen from above. The nimbler player characters can, of course, use this verticality to their advantage — scampering across rooftops, leaping between buildings and drop-killing unsuspecting guards from above.

Each level has a series of *badges* which are revealed to you when you complete them or at the end of your first play-through of a given level. Things like complete the level without touching a bush, without killing anyone or within a certain time limit. I *very* much like that they are only revealed *after* your first play-through since this allows you to play *how you want to play* the first time through, rather than feeling obliged, as we often are despite ourselves, to collect as many badges as we can. First time through you get to just *enjoy* the level as the wonderful playground it is, playing it in your own way. Then, after completing it once, you can return for extra challenges — and they're a lot of fun. This is smart design by developers who care about their game. There's immense replayability here. Not *just* in collecting all the badges but also just trying other approaches, different solutions or using one character more, rather than the other.

{% include img.html image="shadow_tactics_roof.jpg" %}

The artistic direction and design is exquisite throughout. The UI is concise, clear and beautiful, with plentiful tooltips which make learning the controls a breeze. Levels are rendered with a sumptuous naturalism and it's a joy to be able to pan and rotate freely around them. Writing and acting are excellent and cutscenes are stunning, the excellent story is delivered deftly without ever feeling intrusive. Menus are sensible and the main screen, the screen you get when you start the game (from which I took the header image for this post) is *gorgeous*. Little affordances like the quicksave timer, being able to review conversations, the fluency of the controls and the easy access to multiple saves make it plain that someone really *cared* about this game, it feels utterly refined. It's *stunning* that a small team (that I'd never heard of) made this game, it exudes quality.

Shadow Tactics is a game to *savour*. To take your time with and experiment, find better or cooler solutions, different approaches. To *play* with and fully explore.

I don't have a bad word to say about it. And the Linux version is, in my experience, flawless.

Shadow Tactics: Blades of the Shogun is available on [Steam](http://store.steampowered.com/app/418240/), [Humble Store](https://www.humblebundle.com/store/shadow-tactics-blades-of-the-shogun) and [GOG](https://www.gog.com/game/shadow_tactics_blades_of_the_shogun).

There's even a demo which the Shogun *commands* you to try.

