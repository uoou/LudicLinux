---
title: "Stellaris"
image: stellaris_featured.jpg
subtitle: "Oh shit, it's 5am"
author: uoou 
---

Stellaris represents a determined effort on the part of Paradox to take what people love about Crusader Kings II and Europa Universalis IV — the complexity and emergence, the intrigue, the *weirdness* and the sense of telling your own story — and bring it to a wider audience by making it more accessible.

It's a tough thing to do as, to a great extent, what made those games difficult to approach was also what, further down the line, made them interesting. Can Paradox really simplify something like that without compromising its fundamental identity and losing what it essentially *is*?

In short; yeah they can and have. But of course it's a little more complicated than that ...

<!--more-->

{% include img.html image="stellaris_fleet.jpg" caption="Launch every beetleship" %}

Paradox's task was to cut away needless complexity while retaining that which had real ludic purpose. The UI illustrates their success in this regard; it's no longer the bewildering cacophony of icons, numbers and text we grew to love in EU and CK, it's far better organised and does a good job of communicating not just what things *are* but what they *do*. With CKII and EUIV I had to watch tens of hours of videos before I could even begin to play, with Stellaris I just dove right in and things made perfect sense — I even turned off the tutorial tips after about 20 minutes as they were unnecessary and kept talking at me when I was trying to read other stuff.

Stellaris sits *right* on the boundary between grand strategy and 4X, incorporating the best aspects of those two styles of strategy game. It *is* simpler than its historically-based Paradox siblings but it's far more complex and interesting than, say, Civilization V.

## Cockroach

So, let's start where *all* good stories start: with a race of space-faring cockroaches bent on dominating the galaxy.

<img src="/images/stellaris_cockroach.jpg" />

Meet the *Sky Hive*. A race of xenophilic, pacifistic spiritual cockroaches intent, under my guidance, on conquering the galaxy. Or maybe not so much conquering it as friending it into submission.

I love good  character creation systems. I can spend hours in them. The race creation system in Stellaris was pure joy for me, with plenty of options to allow me to make a race that I really felt was *mine*, without being overwhelmed with pointlessly superficial choices. There's a nice balance between cosmetic and meaningful stuff and the system as a whole entices you to role-play despite yourself.

I was chatting with my pal [Matt](https://twitter.com/MatthewJFrith) as we both launched the game for the first time and, while neither of us usually tend to role-play in games, we just couldn't help doing so in Stellaris — me with my hippy cockroaches and him with his racist bird aesthetes who just want to collect all the beautiful things (and people) in the galaxy and bring them back to their celestial nest. Neither of us set out with the intention of creating a race like that, the race creator just *invites* it, encouraging investment right from the start and ensuring that no two playthroughs are going to be quite the same.  

Traits and governmental systems are what really define the character of your race — you get a limited number of trait points to spend on traits, each of which have a moderate and extreme form. Traits are arranged in a circle with those on opposite sides also being philosophically opposed, like xenophilia and xenophobia.

## Planetfall

Stellaris is a strategy game and is, ultimately, about going out into space and controlling planets. You start off with one planet and, based on which FTL propulsion technology you choose during race creation, will be able to travel to a limited number of other star systems. You choose, during character creation, what type of planets your race will be most comfortable on (other types become habitable through tech) and you colonise them by sending out a colony ship, assuming they're not occupied. If they are occupied then you'll want to do something about that and there are various tools at your disposal with that aim in mind.

Planets have, as is usual for spacey strategyey games, a number of slots, determined by planet size, for building things in. Some slots have bonus resources which can be exploited through the usual mines, research facilities, farms and so on.

There's a distinct, but not overwhelming, Star Trek vibe to proceedings. You'll likely begin the game by sending out science ships to scan heavenly bodies, looking for resources and possible locations for colonies. They'll encounter and research various anomalies along the way, make first contact with other civilisations and run into giant space amoebas, space whales, crystalline entities and beings of [pure energy](https://www.youtube.com/watch?v=ijAYN9zVnwg). 

{% include img.html image="stellaris_spacewhale.jpg" caption="Arm photons" %}

My friend [Becca](https://twitter.com/azuresama/) once described Crusader Kings II as "the best Interactive Fiction". I thought that was pretty insightful and it applies equally to Stellaris — the story of your game is defined in large part by random encounters with anomalies and lifeforms which are presented, as they are in other Paradox strategy games, as lovely textual vignettes.

## Making friends

As you expand out into the galaxy you will meet other races. Some will be relatively primitive and, depending on your empire's laws, may be used, abused or helped in various ways. Others will be, like you, taking their first steps into a wider world.

{% include img.html image="stellaris_spacefriends.jpg" caption="When neighbours become good friends <a href=\"https://youtu.be/kIdFzP0TJxc?t=4\">♬</a>" %}

Diplomacy is more accessible than in previous Paradox games, without the huge lists of conditional interactions, but no less flexible or nuanced for it. Your interactions with other races will be characterised by your race's traits, as will theirs. Everything from all out war through trade, access and research agreements to the formation of alliances and, ultimately, federations, is possible. Primitive cultures can be aided, subjugated, enslaved and annexed as is only proper.

While the early exploration phase of the game is wonderful, this is where the real meat is. Stellaris, like its siblings, is not so much about simply gaining territory — that's the easy part — it's about holding it together once you have it. Managing pressures from within and without, choosing relationships, picking sides, allocating resources and choosing where to attack and when. It's about getting as much information as possible and then making the best judgement you can with the information you have. Knowing [when to hold 'em, when to fold 'em, when to walk away and when to run](https://www.youtube.com/watch?v=Jj4nJ1YEAp4).

Fleet management and warfare are simplified compared to those other games which I've already mentioned too much. The interface design is smarter and it's much easier to accomplish what you want to get done. It's really just a matter of selecting your ships and telling them where to go. There are nice convenience touches, like when a science ship is done researching an anomaly it encountered mid-journey it will carry on with its previously assigned tasks without intervention.

Admirals, generals, scientists and, depending on your politics, leaders can be recruited from a pool and assigned to various vessels, colonies and tasks in much the same way as in previous games. They will level up and gain traits as they gain experience.

Technology is presented in a much more interesting — and understandable — way than in previous games. Each time you research a technology you will be presented with a finite selection (three choices in my current game but this can be increased) of technologies to research next — the technology tree is never explicitly presented to the player, you just choose what to research next. I like this approach as it prevents, or at least delays, the weird teleological feel these games sometimes have when you're deciding in 3000 BCE that you'd like to steer your research towards nuclear fusion or whatever. Additional technologies can be unlocked through your encounters in the game; you might learn through your interaction with some space monster how to develop defensive flagella or regenerative hulls, for example. Research is divided into three categories — physics, society and engineering — each of which are treated separately, meaning you don't have to choose between them, they are researched in parallel.

## Boldly claiming

The developers of Stellaris identified, and attempted to fix, a problem common to most strategy games: they become *very dull* during the late game. Usually it's clear who's going to win well before the end and the final portion of the game — which can be many tens of hours of gameplay in games like this — is just a matter of everyone going through the motions.

Stellaris introduces momentous late-game events which can have dramatic, possibly cataclysmic, outcomes on empires. Say you developed robots to the point of sentience but used them as slaves within your empire. Perhaps they'll rise up in revolt, causing chaos and forcing you to give up any external ambitions and focus inwards.

Whether this approach works is yet to be seen. It's going to depend on the events being sufficiently emergent and diverse and, most importantly, not just seeming unfair — if they manage to make it feel like chickens coming home to roost in a bed that you made then it could indeed bring much needed, but fair, unpredictability and dynamism to the late game. It *feels* right to me, but I've not played enough to be certain.

## Space conclusions

I love Stellaris. Paradox have found a nice sweet spot between grand strategy and 4X and parked their game right in it. 

It's not perfect, my main criticism being that it's not *weird* enough. There's weirdness in there, but it doesn't feel as gloriously, surprisingly odd as its siblings can be at times. A lesser issue is that while the devs have done an amazing job with the interface, making intelligent and for the most part successful choices as to what to cut and what to leave in, there are a few areas where I'd like to have a little more complexity and be presented with a little more information. Map modes, for example, would be useful on the galactic map — not as many as CKII and EUIV have, that would be overwhelming — but it'd be handy to see who's allied with whom and things like that.

These are small issues. Paradox have done a stunning job in making an accessible grand strategy game. They've made smart choices as to what to keep in and what to leave out and the result is a game with surprising complexity and depth given how accessible it is. I'm *absolutely certain* that this game will get both better and *more interesting* over time as Paradox follow their usual practise of substantially updating and revising their games after launch with both free patches and paid DLC. I'm also certain that the community will mod it beyond all recognition.

Stellaris isn't a game that's trying to impress you with its technical fidelity but there's a quiet, understated beauty to the graphics which is echoed in the simple elegance of the UI, the beautiful music and dazzlingly gorgeous artwork throughout the game.

{% include img.html image="stellaris_pulsar.jpg" caption="A pulsar showing off" %}

I love what Stellaris is at release and I'm excited to see where the devs and modders take it in the future.

Stellaris is available on [Steam](http://store.steampowered.com/app/281990/?snr=1_7_7_151_150_1) and [Humble](https://www.humblebundle.com/store/stellaris).

If you really want to thank Paradox for their continued, **flawless *day one* Linux support** then I'd suggest buying it via their own [Paradox Plaza](https://www.paradoxplaza.com/stellaris).










