---
title: "Night in the Woods"
image: night_in_the_woods_featured.jpg
subtitle: "Shapes"
---

Night in the Woods is a (largely-)linear-narrative explorationy adventure game — very much not the sort of thing I usually enjoy — but the preview material for this game intrigued me enough to try it and I'm *so* glad that I did.

It's really special.

<!--more-->

Upfront **reverse-quasi-spoiler-warning**: I'll try my very hardest to avoid spoiling anything about this game since much of the enjoyment will come from discovering things for yourself. That's going to make this a bit difficult since I'll be unable to describe the things that affected me the most in anything but the vaguest terms. If you're *reallllly* spoiler-sensitive you might want to stop reading.... **NOW**(ish).

Night in the Woods tells the story of Mae Borowski, an anthropomorphic cat who has just dropped out of college and returned to her small-town home of Possum Springs hoping to pick up her life where she left off. While things are superficially familiar, she finds that the town is changed and her friends have grown up, got jobs and moved on with their lives. The past she wishes to recapture no longer exists.

Possum Springs will be familiar to any of us who've lived in a small provincial town, especially one with an industrial past. The town is decaying, possibly dying, as postmodernity and late capitalism collide, disconnecting the people and place from its history and leaving many of its inhabitants lost — proud of their heritage and community while alienated from its present and unsure of its future.

The town and its surroundings are beautifully rendered both in terms of the gorgeous artwork and through the dialogue of its inhabitants. There's an aching feeling when talking to the townspeople of a place that is still home to many real people with real lives but which has simply outlived its usefulness to a world that's moved on.

{% include img.html image="night_in_the_woods_statue.jpg" %}

Mae is similarly full of character, I've grown rather attached to her during the couple of days we spent together. She's tactless, impulsive, irrepressibly childish and naive. She's also loving, imaginative, fiercely loyal and brave to a fault. She's flawed and all the more loveable for it. She's deeply *human* (I hope that's not offensive to anthropomorphic cat-people) and one of the most complex and interesting characters I've ever encountered in a game. Mae's no longer a child but hasn't yet embraced adulthood — she's struggling to find herself a place in the world.

The game is spent exploring Possum Springs and its surroundings and interacting with its inhabitants including Mae's closest friends. Each day you get to decide which of Mae's friends to hang out with that evening. I won't describe them all, I'll let you get to know them for yourself, but I was particularly drawn to Mae's friend Bea — a chain-smoking, incisively intelligent, overworked alligator. The more time you spend with a particular friend, the more you get to see of their lives and personalities, creating resonances with and adding meaning to later events within the story. Mae and Bea had their ups and downs — I felt quite genuine joy, sadness and guilt during my time with her as each got to know the person the other had become. Every evening spent with Bea felt like a special memory, each in a uniquely distinct way.

{% include img.html image="night_in_the_woods_portrait.jpg" %}

The early game is about getting to know the town and its inhabitants through your relationships with them and the activities you share. Mae's family, the man down the street who told me about the stars, the neighbourhood poet, the weird teens, the normal teens, the creepy kid who likes horror films, the rats, the possum and all the rest. They all feel profoundly alive, vibrant and are deeply affecting. The characterisations are flawless with everyone feeling like a *person* rather than a cliché — adults talk and seem to think like adults and kids are like kids, characters have worries, problems and complexities, and faults and contradictions aren't shied away from or glossed over, they're embraced as what makes us human (or cat, bear, alligator, bird ...).

There's so much I can't say, so many little encounters and events or time spent with characters that meant so much to me and added more meaning to the rest of the game. The writing, art and music are all beautiful and work perfectly together to create a real place where real people live and work, a *community* with a proud history and uncertain future.

Part-way through the game a mystery is uncovered which Mae takes it upon herself to solve. Through doing so she learns even more about the town and gets to know its people even better. That's all I'm saying about that. 

{% include img.html image="night_in_the_woods_garbage.jpg" %}

Night in the woods is a beautiful and moving game. It's about getting to know Mae, her friends and the town she calls her home. It's about relationships — Mae's with her family, friends and neighbours, of course, but also the peoples' with their town and the town's with a changing world. The game doesn't shy away from complexity, embraces ambiguity and, while feeling fully satisfying as a complete narrative, leaves some aspects of its world pleasingly unresolved.

Also there's a mystery <sup>~~about which I will say nothing~~</sup>.

I love this game and Mae is my new favourite computer game character. Bea is my new *second*-favourite computer game character. Unless you're made of stone I think you'll love it too.

You can get Night in the Woods from [Itch](https://finji.itch.io/night-in-the-woods), [Steam](http://store.steampowered.com/app/481510/) and [GOG](https://www.gog.com/game/night_in_the_woods). And absolutely should. Now.







