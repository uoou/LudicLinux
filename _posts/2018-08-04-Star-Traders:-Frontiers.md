---
title: "Star Traders: Frontiers"
image: star_traders_featured.jpg
subtitle: "My God, it's full of stats"
---

Star Traders: Frontiers is a big emergent space-captainy sim that lets you do all the stuff these games always promise but seldom deliver.

Space Tradey games are usually 70% combat, 20% trading and 10% everything else — things like diplomacy and spying are usually limited to scripted narrative encounters. In Star Traders: Frontiers everything has its own dedicated system — diplomacy, spying, exploration, bounty hunting, piracy and so on are all viable ways to play the game in any blending or combination you like.

The game is *absolutely committed* to letting you do whatever you want to do and making it engaging.

<!--more-->

Like the [Trese Brothers](http://www.tresebrothers.com/) [other games](http://www.tresebrothers.com/catalog), Star Traders: Frontiers feels, in some ways, like a throw-back to a time before *indie gaming* was a thing, when independent developers would really take advantage of their freedom from publisher pressure and lovingly craft the game *they* wanted. It's hugely ambitious, uncompromising and very clear about what it wants to be.

It's a very difficult game to describe — aside from anything else, my playthrough will be very different from yours (and my other playthroughs). Let's take a slice through its systems...

Character creation presents you with four starter templates: Pirate, Explorer, Bounty Hunter and Smuggler. But these are just templates and you are free to make your own.

Templates are really just a list of priorities for your new captain. The list items are: Ship, Skills, Experience, Attributes and Contacts. The higher up the list the item is, the more/bigger/higher of it you'll have. If Ship is at the top you'll have a bigger/better equipped ship, if it's second it'll be a little smaller. If contacts is top, you'll have access to more contacts from the start of the game. And so on.

![](/images/star_traders_templates.jpg)

This allows for remarkable freedom in choosing your own starting conditions. If you're going to play in such a way that contacts will be important then put them at the top. If you just want to start in a big ship then go for it. It's a nice way of encouraging experimentation without compromising the high-stakes fun.

You and your crew also have access to twenty-six jobs which can be mixed and matched freely. These jobs give boosts to primary stats and unlock a plethora of passive and active skills which can be used during various phases of the game. Someone with the Diplomat job, for example, will, of course, do better in negotiations but will also increase crew morale, get contacts and military ranks cheaper, mitigate faction relationship penalties after combat and increase relationship gains when completing missions.

What I'm getting at here is that being a diplomat doesn't *just* make you better at conversations and allow you to confidently take diplomacy-based missions — which is what it would do *at very most* in other games — but *also* influences every aspect of the game. You can *meaningfully* play this game as a diplomat (or spy, smuggler, explorer etc.).

{% include img.html image="star_traders_fight.jpg" %}

The game's world, shared with other Trese Brothers games, is a techno-feudal galaxy dominated by noble houses, syndicates and various other factions. Factions each have their own personality which shapes their approach to events. I tend to enjoy the espionagey Rychart Syndicate, for example, but this doesn't mean that I can only do espionage based missions nor that every member of the syndicate is a spy; they just, in a very general way, lean in that direction. Belonging to a particular faction will contextualise but not really limit your dealings with other factions — I often end up being far more pally, due to circumstance, with a faction other than the one with which I started.

Every character you meet in the game will belong to one of these factions and will have a particular career and various traits, some of which will be hidden initially. These characters will all act to further their personal and faction goals and the result of their all doing so determines the broad feel of a particular playthrough.

There are ongoing events and stories in the galaxy which you can engage with as little or as much as you like and which will carry on whether or not you lend a hand. The way these events play out will depend largely on, alongside your engagement, that all inter-factional manoeuvring.

Whether you engage with those big plotlines or not, most of your missions (if you even do missions, I'm sure it's more than possible to play this game without ever taking any) will come from *contacts*. Contacts are really important — alongside giving you missions they'll also, depending on their type, help you increase your military rank within their faction, buy intel, provide you with recruits, sell you trade permits, introduce you to new contacts and all kinds of useful things. You'll form relationships with these people and have favourites, learning more about them the more you work for them, getting drawn into their schemes and getting enjoyably pulled off your intended path as you become embroiled in inter-faction politics. If you want, that is — that's how *my* games go sometimes. Yours will be different.

Star Traders: Frontiers is great at giving you what you want. Its systems weave together organically to draw you deeper into the kinds of stuff you like. It's never *directed*, you never feel like you're being prodded in a particular direction, you'll just naturally become embroiled in plots and escapades, like a kid working for the Mob. And, like that kid, sometimes you'll be in over your head and need to do some quick thinking.

{% include img.html image="star_traders_spy.jpg" %}

The missions themselves are often multi-stage affairs, potentially spanning huge distances and spans of time. Each stage will involve activities appropriate to the mission type — spying on or assassinating a target, delivering contraband to a client, resolving a conflict, exploring a location and so on. These activities are presented as card games with your crew's skills being cards that you can play — the more skills your crew has the more cards you'll have available to you and the better, generally, the mission will go. Ship to ship combat is presented as a turn based RPG style battle, your skill bar made up of all the skills your crew has access to. The personal combat you'll engage in during boarding manoeuvres and ground combat plays out very much like that in Darkest Dungeon; turn based combat with an emphasis on your crew members' positions in the battle line. These mini-games are somewhat abstract but don't feel out of place or synthetic — you really feel like you're using your crew's skills to achieve a goal or overcome an obstacle. Any of these mini-games could be spun out into a respectable stand-alone game, they're *really* fun.

{% include img.html image="star_traders_ship_fight.jpg" %}

Of course you can't have a space captain without a ship. Your ship in Star Traders: Frontiers is *extremely* customisable; if you can think of it (and it makes sense) you can probably make it. Ship components are distinguished by size rather than role; there are big, medium and small components and each hull type has a number of slots for each. This means you can tailor your ship *exactly* to how you want to play. Like to trade and don't plan on fighting? Replace all your weapons with more cargo space. Or do the reverse. Or have amazing sensors. Or a huge fuel capacity. It's very easy to make your ship really feel like *your ship*, designed specifically for you.

I said that Star Traders: Frontiers is a difficult game to describe and it really is. All I've done is outlined some of its systems in the *hope* that I've communicated how free, open and exquisitely designed this game is. But Star Traders: Frontiers is far more than the sum of its systems; it's an utterly immersive, living world that excels at generating unique, personal stories of space adventures, scrapes and escapades. Stories that *belong to you*.

{% include img.html image="star_traders_planet.jpg" %}

During its early access period, Star Traders: Frontiers received almost weekly updates, some of which contained as much content as many games have at launch. At launch it has more content than I can comprehend. The Trese Brothers clearly love what they do and have a very clear idea of exactly what they want to make and why it should be made. They are extremely attentive to their community and were constantly answering questions and responding to requests throughout early access. They're *good developers*.

>We set out to demonstrate exactly how Early Access should be done. We did it the Trese Brothers way – hammering out 87 updates, including huge new features and swathes of content, all without ever invalidating a saved game or pausing more than a few days between big updates. Throughout all this, we’ve also responded to every player thread on Steam. Yes, that might mean we’re crazy – but interacting with our community is extremely important to us.

(quote purloined from [Gaming on Linux](https://www.gamingonlinux.com/articles/the-space-rpg-star-traders-frontiers-from-trese-brothers-games-is-now-out.12281))

Whether you're a Janeway, a Reynolds, a Fett or a Solo, if space adventuring is your thing you should be able to find a home in Star Traders: Frontiers. It's a wonderful game — one of the best I've ever played — and an absolute steal at its price.

Star Traders: Frontiers is available on [Steam](https://store.steampowered.com/app/335620/Star_Traders_Frontiers/) where it's currently on sale at 20% off.

Here's a video of my <span class="jiggle">mortal enemy</span> Hex gushing about Star Traders: Frontiers:

{% include yt.html vidid="Z_SWISLnNkI" %}










