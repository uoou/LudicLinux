---
title: "SUPERHOT"
image: superhot_featured.jpg
subtitle: "It's the most innovative shooter I've played in years"
author: uoou
---

I need to get this out of the way at the start: SUPERHOT is *beautiful*.

I'm partial to constructivist and suprematist art so maybe I'm influenced by that — the explosions of red polygons, the solid black weapons and white ground make screenshots look like artworks by [Rodchenko](https://en.wikipedia.org/wiki/Alexander_Rodchenko), [Lissitzky](https://en.wikipedia.org/wiki/El_Lissitzky) or [Malevich](https://en.wikipedia.org/wiki/Kazimir_Malevich) — but this game really is aesthetically stunning. There've been times when I've died because I was so distracted by the beauty of the scene before me that I failed to notice the bullet about to enter my face.

SUPERHOT is a first person shooter with the central conceit that time only moves when you do (which isn't *quite* true, time moves very very slowly when you're stationary) allowing the player to pull off some crazy John Woo-esque moves that wouldn't be possible in a normal shooter. Punch a man in the face, grab his gun from the air as he drops it, shoot him with his own gun, turn 180°, throw the gun in the air and punch the man who was behind you in the face after side-stepping the bullet he just fired, turn to your left and catch the gun you threw, shoot the man to your left and so on.

<!--more-->

The FPS is transformed into something between turn based combat and a puzzle game without losing the raw excitement of second-to-second first person combat.

{% include img.html image="superhot_constructivist.jpg" caption="After you've been playing for a while you'll swear that screenshots are moving slowly" %}

It's a simple mechanic but it's artfully executed; animations are fluid, physical and weighty — it *feels* good as you perform superhuman feats — and the intent of your AI opponents is evident from their posture and movement. The sparse aesthetic, aside from just looking gorgeous, keeps the game readable — visual confusion would've spoiled the flow. Combat sounds have a pleasantly chunky feel to them and attenuate wonderfully as you speed up and slow down time.

SUPERHOT has something like a story; it's more of a meta-narrative with shades of [Stanley Parable](http://store.steampowered.com/app/221910/) (though far less extreme) and The Matrix to it. I wouldn't quite call it a narrative — nothing is really expounded, developed or resolved, it's more like flavour text and I like it all the more for that. This meta-narrative is mainly delivered through the glitchy faux text-based OS that constitutes the game's menu system and comprises IMs, IRC-like chats and mini-games (most of which are optional). I'm not usually a fan of superfluous narrative in action games but I enjoyed the meta-narrative of SUPERHOT; it's not deep, but it's intriguing, sometimes amusing and, a couple of times, pleasantly disturbing. It augments the action, adding a slight emotional layer, without distracting from it.

{% include img.html image="superhot_ouch.jpg" caption="SUPER. HOT. SUPER. HOT." %}

The main story/campaign of SUPERHOT will only take you a couple of hours to complete. It took me about three and a half and I'm *slow*. I've seen complaints about this but I think they miss the point — completing the main campaign unlocks the rest of the game and *that* is where the real meat is — the main campaign is *really* just an extended tutorial. I'll happily play the same level for hours, trying to beat my score or complete a challenge or challenging myself to play in a particular way (fists and thrown-things only, for example). If you *don't* enjoy that sort of gameplay then I'd look elsewhere since that's the game this is — you're intended to replay and refine, it's very much a toy to be played *with* as well as being a game to play *through*.

{% include img.html image="superhot_system.jpg" caption="It's the most innovative shooter I've played in years" %}

Having said that, I do hope the developers add more content. There's enough here to keep me busy honing my superhuman ninja combat skills for quite a while, but there will definitely come a time when I want more. More maps and possibly more weapons to play with too. It feels like the sort of game that could be expanded upon for a long time and grow into something even more special than it already is, I hope the devs take that route.

This is going to be my go-to relaxation game for a while, I think. I recommend it!

SUPER. HOT. 

(Also, it's beautiful.)

SUPERHOT is available for Linux on [Steam](http://store.steampowered.com/app/322500/) and [GOG](http://www.gog.com/game/superhot).

Here's a video I made of a few minutes of Endless Mode gameplay:

{% include yt.html vidid="yFU_qveM2V0" %}
