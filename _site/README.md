# Ludic Linux

[ludiclinux.com](http://ludiclinux.com/)

## Why?

There are plenty of Linux gaming news sites but I wanted a site that is *just* about the games, not the tech and not news.

Old games, new games, good games, bad games - it doesn't matter so long as they are *interesting games* and they run on Linux.

A place to discover *interesting* games for Linux and hear about *why* they're interesting. Not the AAA stuff that's covered well elsewhere but stimulating games that readers may have overlooked or dismissed.

## What?

The site's hosted on ~~Github~~ Gitlab using ~~Github~~ Gitlab Pages. The code, such that it is, is licensed under GPLv3. Post texts are licensed under a Creative Commons BY-SA license.

Hosting on ~~Github~~ Gitlab means that anyone can submit an article. The license means that anyone can take those articles and do what they like with them (so long as they give credit). This seems in-keeping with the spirit and culture of Linux and Free Software.

The site won't have:

* ads
* trackers/analytics
* javascript

## How?

Articles **will** be curated, I won't publish articles I (or whoever else ends up in a curation role) believe to be uninteresting or poorly written.

## Where?

I already said, [ludiclinux.com](http://ludiclinux.com/).

## When?

Now.

## Wherefore?

*Therefore*.



